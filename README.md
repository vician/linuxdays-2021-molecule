# LinuxDays 2021 Molecule

## Demo

### Install dependencies
```
python3 -m pip install molecule docker molecule-docker
```

### Init molecule
```
cd roles
molecule init role -d docker nginx0
```

### Simple test on Centos8

```
cd roles/nginx1
molecule test
```

### Multiple platforms

```
cd roles/nginx2
molecule test
```

### Automatic testing

```
cd roles/nginx3
molecule test
```

https://gitlab.com/vician/linuxdays-2021-molecule/-/pipelines
